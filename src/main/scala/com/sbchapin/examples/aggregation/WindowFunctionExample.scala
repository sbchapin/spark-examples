package com.sbchapin.examples.aggregation

import com.sbchapin.SparkMain
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._


object WindowFunctionExample extends SparkMain {
  override val exampleName: String = this.getClass.getSimpleName
  import spark.implicits._

  /**
    * Create a list of letters and numbers.  Each letter will be repeated as many letters into the alphabet it is.
    *
    * @return The letters of the alphabet, with duplicates.
    */
  def createSampleData(): Seq[(String, Int)] = {
    val alphabet = "abcdefghijklmnopqrstuvwxyz"
    for {
      letter <- 'a' to 'z'
      number <- 1 to (alphabet.indexOf(letter) + 1)
    } yield {
      (letter.toString, number)
    }
  }

  /**
    * Entry point, showing usage of a Window function.
    *
    * @param args Ignored
    */
  override def main(args: Array[String]): Unit = {

    val alphaNumericDataframe =
      spark.createDataFrame(createSampleData())
        .withColumnRenamed("_1", "letter")
        .withColumnRenamed("_2", "number")
    log { alphaNumericDataframe }

    // Basic aggregation of data
    log {
      alphaNumericDataframe
        // Apply window function to append the sum of all numbers by letter, on each letter.
        .withColumn("letterSum", sum('number).over(Window.partitionBy('letter)))
        // Apply window function to append the lowest ordinal letter of all letters by number, on each number.
        .withColumn("lowestLetter", first('letter).over(Window.partitionBy('number).orderBy('letter)))
        // Sort the output to make it easier to understand
        .orderBy('letter, 'number)
    }

    // More complex aggregation of data, involving using "previous rows"
    log {
      alphaNumericDataframe
        // Apply window function to get the pprevious row's value
        .withColumn("previousValue", lag('number, offset = 1, defaultValue = 0).over(Window.orderBy('letter, 'number)))
        // Use the window column to calculate the difference
        .withColumn("differenceFromPrevious", 'number - 'previousValue)
        // Sort the output to make it easier to understand
        .orderBy('letter, 'number)
    }

  }
}
