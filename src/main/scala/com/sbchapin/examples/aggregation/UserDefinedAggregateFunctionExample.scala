package com.sbchapin.examples.aggregation

import com.sbchapin.SparkMain
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._


object UserDefinedAggregateFunctionExample extends SparkMain {
  override val exampleName: String = this.getClass.getSimpleName
  import spark.implicits._

  /**
    * Create a list of letters, each with a map of letters and numbers.  Each letter will be repeated as many letters into the alphabet it is.
    *
    * @return The letters of the alphabet, with duplicates.
    */
  def createSampleData(): Seq[(String, Map[String, Int])] = {
    val alphabet = "abcdefghijklmnopqrstuvwxyz"
    for {
      outerLetter <- 'a' to 'z'
      number <- 1 to (alphabet.indexOf(outerLetter) + 1)
    } yield {
      val innerLetterNumberPairs = for (innerLetter <- outerLetter to 'z') yield (innerLetter.toString, number)
      (outerLetter.toString, innerLetterNumberPairs.toMap)
    }
  }

  /**
    * Entry point, showing usage of a UserDefinedAggregateFunction.
    *
    * @param args Ignored
    */
  override def main(args: Array[String]): Unit = {

    val alphaNumericDataframe =
      spark.createDataFrame(createSampleData())
        .withColumnRenamed("_1", "letter")
        .withColumnRenamed("_2", "numberMap")
    log { alphaNumericDataframe }

    // Example using the UDAF defined below, doing a "semigroup" operation on grouped maps
    val addingSemigroup = new Semigroup(_ + _)
    val subtractingSemigroup = new Semigroup(_ - _)
    log {
      alphaNumericDataframe
        // Standard spark SQL group statement
        .groupBy('letter)
        // Standard spark SQL agg statement
        .agg(
          // Standard spark SQL aggregate function
          count('*) as 'numberOfConsolidatedMaps,
          // Custom UDAFs
          addingSemigroup('numberMap) as 'addedMaps,
          subtractingSemigroup('numberMap) as 'subtractedMaps
        )
        // Sort the output to make it easier to understand
        .orderBy('letter)
    }
  }

  /**
    * The UserDefinedAggregateFunction that we control
    */
  class Semigroup(inersectionOperation: (Int, Int) => Int) extends UserDefinedAggregateFunction {

    // Schema of the object(s) we hold while aggregating
    override def bufferSchema: StructType = StructType(Seq(StructField("map", dataType)))

    // Schema of the input to be aggregated
    override def inputSchema: StructType = StructType(Seq(StructField("map", dataType)))

    // Schema of the aggregated output
    override def dataType: DataType = MapType(StringType, IntegerType)

    // Whether this function is deterministic or not.  Can affect performance.
    override def deterministic: Boolean = true

    override def initialize(buffer: MutableAggregationBuffer): Unit = {
      // initialize buffer with empty map
      buffer(0) = Map.empty[String, Int]
    }

    // Merge two partitions (buffers) together
    override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = update(buffer1, buffer2)

    // Update partition with new row information
    override def update(buffer: MutableAggregationBuffer, row: Row): Unit = {
      // update buffer with new values
      if (!row.isNullAt(0)) {
        buffer(0) = semigroup(
          buffer.getMap[String, Int](0).toMap, // buffer value
          row.getMap[String, Int](0).toMap // row value
        )
      }
    }

    // Final operation to gather contents of buffer
    override def evaluate(buffer: Row): Any = buffer.getMap[String, Int](0)

    // our definition of what a "semigroup" operation does
    /**
      * Union two maps, summing where they intersect.
      *
      * @param l First map to union with second
      * @param r Second map to union with first
      * @return Semigrouped map
      */
    private def semigroup(l: Map[String, Int], r: Map[String, Int]): Map[String, Int] = {
      l ++ r.map { case (k, v) => k -> inersectionOperation(v, l.getOrElse(k, 0)) }
    }
  }
}
