package com.sbchapin

import com.sbchapin.examples.aggregation.WindowFunctionExample.logger
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Base class all examples come from
  */
abstract class SparkMain extends LazyLogging {
  val exampleName: String

  // Spark initialization
  lazy val spark: SparkSession =
    SparkSession
      .builder()
      .appName(exampleName)
      .master("local")
      .config("spark.sql.shuffle.partitions", 5) // to speed up examples, don't use in prod
      .getOrCreate()

  def log (dataFrame: DataFrame): Unit = {
    val collection = dataFrame.collect()
    logger.debug(
      s"""
         |Schema:
         |  ${dataFrame.schema.simpleString}
         |Count:
         |  ${collection.length}
         |Data:
         |  ${collection.mkString("\n  ")}
       """.stripMargin
    )
  }

  def main(args: Array[String]): Unit

}
